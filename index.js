const express = require("express")

// app - server
const app = express()

const port = 3000

// Middlewares - software that provides common services and capabilities to applications outisde of what's offered by the operating system

// allows your app to read JSON data
app.use(express.json())

// allows your app to read data from any other forms
app.use(express.urlencoded({extended: true}))

// [SECTION] ROUTES
// GET method

app.get("/greet", (request, response) => {
		response.send("Hello from the /greet endpoint")
	})

// POST method

app.post("/hello", (request,response) => {
		response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`)
})

// Simple registration form

let users = []

app.post("/signup", (request,response) => {

	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		response.send(`User ${request.body.username} succesfully registered`)
	}else {
		response.send("Please input BOTH username and password")
	}
})

// Simple change password transcation

app.patch("/change-password", (request, response) => {

	let message

	for(let i = 0; i < users.length; i++){
		if(request.body.username == users[i].username){
			users[i].password = request.body.password
			message = `User ${request.body.username}'s password has been updated!`

			break
		}else {
			message = "User does not exist"
		}
	}




	response.send(message)

})



// [SECTION] Activity

// Welcome page

app.get("/home", (request, response) => {
		response.send("Welcome to the home page")
	})

// Users

app.post("/users", (request,response) => {
		response.send(`Username: ${request.body.userName}
		 password: ${request.body.password}!`)
})

// Delete user
app.delete("/delete-user", (request,response) => {
		response.send(`User ${request.body.userName} has been deleted`)
})

app.listen(port, () => console.log(`Server running a ${port}`))